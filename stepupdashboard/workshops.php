
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<title>Stepup Hospitality</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="Ansu Tech">
<meta name="author" content="Pranil GC">
<meta name="Description" content="Stepup Hospitality" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/contact.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
<link href="../css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="../css/responsive.css" rel="stylesheet" type="text/css" media="screen" />
<link href="http://fonts.googleapis.com/css?family=Oswald:400,600,700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Lora:400,400italic,600" rel="stylesheet" type="text/css" />
<script src="../js/modernizr.custom.js" type="text/javascript"></script>
   
</head>

<body class="blog">
<!-- start half-page intro -->
<section class="bg-image-4 with-bg parallax parallax1 section header-section">
  <div class="overlay">
    <div class="parent">
      <div class="child">
        <h1>Cocktail Workshop <span class="italic">&amp;</span> Barista Workshop</h1>
        <p class="large">Course offered!</p>
      </div>
    </div>
  </div>
</section>
<!-- end half-page intro --> 
<!-- start header -->
<header class="clearfix">
  <div id="logo"> <a href="index.html">Stepup</a> </div>
  <div class="tagline"><span>Stepup Hospitality</span></div>
  <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
  <nav>
    <ul id="nav">
      <li><a href="../index.html#ancor1">Home</a> </li>
      <li><a href="../index.html#ancor3">Reservation</a> </li>
      <li><a href="../index.html#ancor4">About</a> </li>
      <li><a href="displayevent.php">Events</a></li>
      <li><a href="#cocktail-workshop">Cocktail Workshop</a></li>
      <li><a href="#barista-workshop">Barista Workshop</a></li>
      <li><a href="../index.html#ancor6">Contact</a> </li>
      <li><a href="../gallery.html">Gallery</a> </li>
    </ul>
  </nav>
</header>
<!-- end header --> 
<!-- start main content -->
<section >
       
  <div class="container-fluid">
         <div class="row">
          
           <div class="col-md-9">
                  <!-- ----start cocktail syllabus (fetching from database and displaying on page) -->

                  <div class="card mb-2" style="border:1px solid #eee;"   id="cocktail-workshop" >
                    <div class="card-header bg-primary text-center">
                      <h2 style="color:white">Cocktail Workshops</h2>
                    </div>
                  </div>

                  <?php 
                include "../config/dbconfig.php";
                $sql ="SELECT * FROM syllabus where course= 'cocktail' ";
                $result =mysqli_query($conn,$sql);
                if($result){
                    while($row = mysqli_fetch_assoc($result)){
                      $id=$row['id'];
                $image=$row['image'];
                $course =$row['course'];
                $title=$row['title'];
                $description =$row['description'];
                $resource =$row['resource'];
            ?>   
            
          <div class="card col-md-5 col-sm-12 text-center"  style="border:2px solid #eee;margin:2.5%">
            <div class="card-body " style="height:300px;" > 
            <img style="height:300px;width:100%;" src="<?php echo $image;?>" >
            
            </div>
            <div class="card-footer text-muted">
            <h5 class=" text-success text-white spinner-grow"><?php echo "Title: ".$title ?></h5>
           
                                 
            <a href="syllabusdetails.php?id=<?php  global $id; echo $id; ?>" class="btn btn-warning text-white" style="margin-bottom:10px;">view details</a> 
            </div>
            
         </div>
        
          <?php 
                    }
                }
          ?>
           </div> 
          <!-- end cotktail section -->

          <!-- start listing syllabus dynamically -->
          <div class="col-md-3 col-sm-5 mt-2">
      <!-- <input name="name" type="text" id="name" value="Search ..." class="search-widget" /> -->
      <aside>
        <div style="text-align:justify">
          <h5>About</h5>
          <p>Step up Hospitality ,a cocktail & Barista workshop, is a young place to promote a local bartenders as well as to polish a future bartenders in this global market.</p>
        </div>
     </aside>
       <!-- start listing cocktail syllabus dynamically -->
      <aside>
        <h5>Cocktail Workshops</h5>
           <?php 
                // $conn = mysqli_connect("localhost","root","","stepup");
                $sql ="SELECT * FROM syllabus where course = 'cocktail' ";
                $result =mysqli_query($conn,$sql);
                if($result){
                    while($row = mysqli_fetch_assoc($result)){
              
                $title=$row['title'];
                $id=$row['id'];

            ?> 
          <ul class="unordered-list clearfix">
            <li ><a href="syllabusdetails.php?id=<?php  global $id; echo $id; ?>" style="color:blue"><?php echo $title ?></a></li>
          </ul>
          <?php 
                    }
                }
      ?>
      </aside>
      
       <!-- end listing cocktail syllabus dynamically -->

       <!-- start listing barista syllabus dynamically -->
      <aside>
        <h5>Barista Workshops</h5>
           <?php 
                // $conn = mysqli_connect("localhost","root","","stepup");
                $sql ="SELECT * FROM syllabus where course = 'barista' ";
                $result =mysqli_query($conn,$sql);
                if($result){
                    while($row = mysqli_fetch_assoc($result)){
            
                $title=$row['title'];
                $id=$row['id'];

            ?> 
          <ul class="unordered-list clearfix">
            <li><a href="syllabusdetails.php?id=<?php  global $id; echo $id; ?>" style="color:blue"><?php echo $title ?></a></li>
            
          </ul>
          <?php 
                    }
                }
      ?>
      </aside>
     
      <!-- end listing barista syllabus dynamically -->
     
       </div>
          <!-- end listing syllabus -->

          <!-- start displaying barista syllabus -->
          <div class="col-md-9 ">
          <div class="card mb-2" style="border:1px solid #eee;"   id="barista-workshop" >
                    <div class="card-header bg-primary text-center">
                      <h2 style="color:white; display:block">Barista Workshop</h2>
                    </div>
            </div>

                  <?php 
              //  $conn = mysqli_connect("localhost","root","","stepup");
                $sql ="SELECT * FROM syllabus where course= 'barista' ";
                $result =mysqli_query($conn,$sql);
                if($result){
                    while($row = mysqli_fetch_assoc($result)){
                  $id=$row['id'];
                $image=$row['image'];
                $course =$row['course'];
                $title=$row['title'];
                $description =$row['description'];
                $resource =$row['resource'];

            ?>   
          <div class="card col-md-5 col-sm-12 text-center"    style="border:2px solid #eee;margin:2.5%">
            <div class="card-body " style="height:300px;" > 
            <img style="height:300px;width:100%;" src="<?php echo $image;?>" >
                </div>
            <div class="card-footer text-muted">
            <h5 class=" text-success text-white spinner-grow"><?php echo "Title: ".$title ?></h5>
            
                           <a href="syllabusdetails.php?id=<?php  global $id; echo $id; ?>" class="btn btn-warning text-white" style="margin-bottom:10px;">view details</a>
            
            </div>
            
         </div>
        
          <?php 
                    }
                }
          ?> 
           </div>
          <!-- end barista syllabus -->
            
       </div>
 </div>
    
</section>
<!-- end main content --> 
<!-- start footer -->
<footer  style="margin-top:5%">
  <div class="container clearfix">
    <div class="col-lg-12"> <span class="alignleft small">© 2020 Stepup Hospitality. All Rights Reserved.</span> <span class="alignright small">Made with <i class="fa fa-heart"></i> by <a href="https://www.ansutechnology.com.np" data-title="Software company of Nepal">Ansu Tech</a>. </span> </div>
  </div>
</footer>
<!-- end footer --> 
<script src="../js/jquery-1.12.4.min.js" type="text/javascript"></script> 
<script src="../js/jquery-easing-1.3.js" type="text/javascript"></script> 
<script src="../js/jquery.touchSwipe.min.js" type="text/javascript"></script> 
<script src="../js/jquery.isotope2.min.js" type="text/javascript"></script> 
<script src="../js/packery-mode.pkgd.min.js" type="text/javascript"></script> 
<script src="../js/jquery.isotope.load.js" type="text/javascript"></script> 
<script src="../js/jquery.nav.js" type="text/javascript"></script> 
<script src="../js/responsive-nav.js" type="text/javascript"></script> 
<script src="../js/jquery.sticky.js" type="text/javascript"></script> 
<script src="../js/jquery.form.js" type="text/javascript"></script> 
<script src="../js/starter.js" type="text/javascript"></script> 
<script src="../js/jquery.flexslider-min.js" type="text/javascript"></script> 
<script src="../js/ajax.js"></script> 
<script src="../js/bootstrap.min.js"></script> 
<script src="../js/bootstrap-datepicker.min.js"></script> 
<script src="../js/jquery.fitvids.js" type="text/javascript"></script> 
<script src="../js/jquery.fancybox.pack.js" type="text/javascript"></script> 
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> 
<script src="../js/googlemaps.js" type="text/javascript"></script> 
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</body>
</html>
