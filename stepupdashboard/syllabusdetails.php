<?php 
$id = $_GET['id']; //why doesn't it work with post method


?>

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<title>Syllabus details</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="author" content="Ansu Tech">
<meta name="author" content="Pranil GC">
<meta name="Description" content="Stepup Hospitality" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/contact.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
<link href="../css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="../css/responsive.css" rel="stylesheet" type="text/css" media="screen" />
<link href="http://fonts.googleapis.com/css?family=Oswald:400,600,700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Lora:400,400italic,600" rel="stylesheet" type="text/css" />
<script src="../js/modernizr.custom.js" type="text/javascript"></script>
   
</head>

<body class="blog">

<!-- start header -->
<header class="clearfix">
  <!-- <div > <a href="index.html">Stepup</a> </div> -->
  <div class="tagline"><span>Stepup Hospitality</span></div>
  <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
  <nav>
    <ul id="nav">
      <li><a href="../index.html#ancor1">Home</a> </li>
      <li><a href="../index.html#ancor2">Rooms</a> </li>
      <li><a href="../index.html#ancor3">Reservation</a> </li>
      <li><a href="../index.html#ancor4">About</a> </li>
      <li><a href="#cocktail-workshop">Cocktail Workshop</a></li>
      <li><a href="#barista-workshop">Barista Workshop</a></li>
      <li><a href="../index.html#ancor6">Contact</a> </li>
      <li><a href="../gallery.html">Gallery</a> </li>
    </ul>
  </nav>
</header>
<!-- end header --> 
<!-- start main content -->
<section class="">
       
  <div class="container-fluid">
         <div class="row">

         <?php 
                include "../config/dbconfig.php";
                $sql ="SELECT * FROM syllabus where id =$id ";
                $result =mysqli_query($conn,$sql);
                if($result){
                    while($row = mysqli_fetch_assoc($result)){
                    
                $image=$row['image'];
                $course =$row['course'];
                $title=$row['title'];
                $description =$row['description'];
                $resource =$row['resource'];

            ?> 

           <div class="col-md-12 col-sm-12">
                  <!-- ----start cocktail syllabus (fetching from database and displaying on page) -->

                  <div class="card mb-2" style="border:1px solid #eee;" >
                    <div class="card-header bg-primary text-center">
                      <h2 style="color:white"><?php echo $title?></h2>
                    </div>
                  </div>

              <div class="col-md-12 col-sm-12 text-center"  style="border:2px solid #eee;margin-top:10px">
           <div class="col-md-8">
           <div class="text-muted">

                      <p style="text-align:justify"><?php echo $description?></p>
               
            </div>
           </div>
            <div class="col-md-4">
                     <div class=" " style="height:300px;" > 
                 <img style="height:300px;width:100%;" src="<?php echo $image;?>" >
               </div>
            </div>
            
         </div>
                  <?php 
                    }
                }
          ?>
           </div> 
       </div>
 </div>
    
</section>
<!-- end main content --> 
<!-- start footer -->
<footer>
  <div class="container clearfix">
    <div class="col-lg-12"> <span class="alignleft small">© 2020, Stepup Hospitality. All Rights Reserved.</span> <span class="alignright small">Made with <i class="fa fa-heart"></i> by <a href="http://www.weibergmedia.com" data-title="Premium HTML5 Website Templates">Ansu Tech</a>. </span> </div>
  </div>
</footer>
<!-- end footer --> 
<script src="../js/jquery-1.12.4.min.js" type="text/javascript"></script> 
<script src="../js/jquery-easing-1.3.js" type="text/javascript"></script> 
<script src="../js/jquery.touchSwipe.min.js" type="text/javascript"></script> 
<script src="../js/jquery.isotope2.min.js" type="text/javascript"></script> 
<script src="../js/packery-mode.pkgd.min.js" type="text/javascript"></script> 
<script src="../js/jquery.isotope.load.js" type="text/javascript"></script> 
<script src="../js/jquery.nav.js" type="text/javascript"></script> 
<script src="../js/responsive-nav.js" type="text/javascript"></script> 
<script src="../js/jquery.sticky.js" type="text/javascript"></script> 
<script src="../js/jquery.form.js" type="text/javascript"></script> 
<script src="../js/starter.js" type="text/javascript"></script> 
<script src="../js/jquery.flexslider-min.js" type="text/javascript"></script> 
<script src="../js/ajax.js"></script> 
<script src="../js/bootstrap.min.js"></script> 
<script src="../js/bootstrap-datepicker.min.js"></script> 
<script src="../js/jquery.fitvids.js" type="text/javascript"></script> 
<script src="../js/jquery.fancybox.pack.js" type="text/javascript"></script> 
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> 
<script src="../js/googlemaps.js" type="text/javascript"></script> 
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</body>
</html>
