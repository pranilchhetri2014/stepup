<!-- for cocktail -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>cocktail</title>
    <!-- css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<style>

    
div.relative {
  position: relative;
  width:auto;
  height: 100%;
  border: 3px solid #73AD21;
} 

div.absolute {
  position: absolute;
  width: auto;
  height: 100%;
  border: 3px solid #73AD21;
}

</style>
</head>
<body>
cocktail
    <div class="container ">
       <div class="row">
                <?php 
                    include "../config/dbconfig.php";
                    $sql ="SELECT * FROM syllabus where course= 'cocktail' ";
                    $result =mysqli_query($conn,$sql);
                    if($result){
                        while($row = mysqli_fetch_assoc($result)){
                    $image=$row['image'];
                    $course =$row['course'];
                    $title=$row['title'];
                    $description =$row['description'];
                    $resource =$row['resource'];

          
                ?>  
            <div class="card col-md-5 ml-1 mr-1 mt-2 text-center">
                <div class="card-header relative ">
                    <img src="<?php echo $image;?>" class="absolute" height="100%" width="100%">
                    
                                     
                </div>
                <div class="card-body "> 
                <h5 class=" text-success text-white spinner-grow"><?php echo "Title: ".$title ?></h5>
                
                    <span class="text-justify"><?php echo"Description:".$description?></span>
                    <p ><?php echo "resource:".$resource ?></p>
                </div>
                <div class="card-footer text-muted">
                <a href="" class="btn btn-warning text-white">view details</a>
                </div>
                
            </div>
            
            <?php 
                        }
                    }
            ?>                
        </div>
   </div>
</body>
</html>