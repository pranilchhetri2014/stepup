<?php 
session_start();
if(!isset($_SESSION['username'])){
    header('location:../login/login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard: Stepup_Hospitality</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">stepup hospitality</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

     

      <!-- Divider -->
      <hr class="sidebar-divider">

      
      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>
      
      
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="event.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span> Events</span></a>
    </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item active">
        <a class="nav-link" href="syllabus.php">
          <i class="fas fa-fw fa-table"></i>
          <span>syllabus</a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

                  <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

          

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">admin</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              
                
                <a class="dropdown-item" href="../login/logout.php" >
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->
        <!-- upload syllabus-- -->
        <div class="container-fluid bg-primary  ">
          <div class="row">
            <div class="col-md-12">
             <h2 class="text-white ">  Add new syllabus</h2>
            </div>
          </div>
        </div>
        <!-- End of Topbar -->
        <!-- Upload new event form -->
    <div class="container-fluid">
      <div class="row">
       <div class="col-md-6">
         <form action="syllabusaction.php" method="post" enctype="multipart/form-data" class="mt-4">
          <div class="form-group form-inline">
            <label for="cars">course: &emsp;&emsp;</label>
              <select  class="form-control" name ="course">
                <option value="cocktail">Cocktail</option>
                <option value="barista">Barista</option>
             </select>
          </div>
            <div class="form-group form-inline">
              <label for="ttl">Title:&emsp; &nbsp; &nbsp; &emsp;</label>
              <input type="text" id="ttl" name="title" class="form-control" required>
            </div>
            <div class="form-group form-inline">
              <label for="img">Upload file:  &nbsp;  </label>
              <input style=" max-width:50%; height:auto;"type='file' accept='image/*' name="file" id="img" class="form-control" required>
           </div>
            <div class="form-group form-inline">
              <label for="resources">Description &nbsp; </label>
              <textarea name="description" id="description" cols="30" rows="3" class="form-control">
              </textarea>
            </div>
    
             <div class="form-group form-inline">
                <label for="resource">Important resources &nbsp; </label>
                <textarea name="resource" id="resource" cols="30" rows="3" class="form-control">
                </textarea>
              </div>
            
            <input type="submit" value="upload" name="submit" class="btn btn-primary" >
          </form>
       </div>
      </div>
    </div>

    <div class="container-fluid bg-primary mt-4">
      <div class="row">
        <div class="col-md-12">
         <h2 class="text-white ">Existing syllabus</h2>
        </div>
      </div>
    </div>
        <!-- end of upload syllabus --- -->
        <div class="container-fluid">
      <div class="row col-md-12">
      <table class="table table-bordered">
               <thead>
                    <tr>
                      <th scope="col">S.N</th>
                      <th scope="col">Course</th>
                      <th scope="col">Title</th>
                      <th scope="col">Image</th>
                      <th scope="col">Description</th>
                      <th scope="col">Resource</th>
                      <th scope="col">Action</th>
                    </tr>
               </thead>

                <tbody >
                <?php 
                   include "../config/dbconfig.php";
                   $sql ="SELECT * FROM syllabus";
                   $runquery = mysqli_query($conn,$sql);
                   
                       while($result = mysqli_fetch_array($runquery)){
                        ?>
                      
                <!--display event in dashboard ---------------------- -->
              
                    <tr >
                        <th scope="row"><?php  echo $result['id']; ?></th>
                        <td><?php echo $result['course']; ?></td>
                        <td><?php echo $result['title']; ?></td>
                        <td> <img src="<?php echo $result['image'];?>" height="50px" width="50px"> </td>
                        <td><?php echo $result['description']; ?></td>
                        <td><?php echo $result['resource']; ?></td>
                       
                        <td>
                           <a href="deletesyllabus.php?id=<?php echo $result['id'] ?>"><span style="color:red ">Delete</span></a> 
                        </td>
                    </tr>
                    <?php 
                       }
                   
           ?>
                                    </tbody>
                </table>
                <!-- -- -->
                
           <!-- ---------------------- -->
                     
                          
      </div>
    </div>

      </div>
      <!-- End of Main Content -->

     <!-- Footer -->
     <<footer style="margin-top:5% ">
  <div class="container clearfix bg-secondary text-white text-center">
    <div class="col-lg-12"> <span class="alignleft small "> &copy;2020, Stepup Hospitality. All Rights Reserved.</span> <span class="alignright small">Made with <i class="fa fa-heart"></i> by <a  class="text-white"  href="" >Ansu Tech</a>. </span> </div>
  </div>
</footer>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
